resource "local_file" "test_file" {
  filename = "test.txt"
  content = "Test succeeded. See build_spec Terraform output to verify that your post-build command has been injected sucessfully, and check the last Git log message in git_log above."
}

data "local_file" "test_file_check" {
  filename = "test.txt"
  depends_on = ["local_file.test_file"]
}

data "local_file" "buildspec_check" {
  filename = "buildspec.yml"
}

data "external" "gitlog_check" {
  program = ["bash", "-c", "echo -n \"{ \\\"result\\\": \\\"\\\\n$(git log -1 | tr '\n' '|' )\\\"} \" | sed 's/|/\\\\n/g'"]
}

output "result" {
  value = "${data.local_file.test_file_check.content}"
}

output "build_spec" {
  value = "${data.local_file.buildspec_check.content}"
}

output "git_log" {
  value = "${data.external.gitlog_check.result.result}"
}
